﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Atlatl.Test.Part;

namespace Atlatl.Test.UI 
{
    /// <summary>
    /// UI Button representation of part that can be selected
    /// </summary>
    public class PartUiItem : MonoBehaviour
    {
        public Text nameLabel;
        public Action<PartUiItem> PartSelected;
        public Button partButton;
        PartSelector.ButtonStateColors buttonStateColors;

        string _partName;
        public string PartName
        {
            get { return _partName; }

            set
            {
                _partName = value;
                nameLabel.text = value;
            }
        }

        PartBase _part;
        public PartBase Part
        {
            get { return _part; }

            set
            {
                _part = value;
                PartName = FixPartName(_part.name);
            }
        }

        public void SetColors(PartSelector.ButtonStateColors colors)
        {
            buttonStateColors = colors;
        }

        void Start()
        {
            SetupEvents();
        }

        void OnDestroy()
        {
            DisposeEvents();
        }
        
        /// <summary>
        /// Changes the state of the button to incompatible
        /// </summary>
        public void MarkAsIncompatible()
        {
            partButton.image.color = buttonStateColors.incompatibleState; 
            partButton.interactable = false;
        }

        /// <summary>
        /// Changes the state of the button to selected
        /// </summary>
        public void MarkAsCurrentPart()
        {
            partButton.image.color = buttonStateColors.selectedState;
            partButton.interactable = false;
        }

        /// <summary>
        /// Changes the state of the button to unselected/normal
        /// </summary>
        public void MarkAsNormal()
        {
            partButton.image.color = buttonStateColors.normalState;
            partButton.interactable = true;
        }

        /// <summary>
        /// Takes the model part name and converts it into a nice readable format to display in the UI
        /// </summary>
        string FixPartName(string partName)
        {
            if (!partName.Contains("_")) return partName;
            System.Text.StringBuilder partNameBuilder = new System.Text.StringBuilder();
            partNameBuilder.Append(StaticPart.GetModelNameInfo(partName, StaticPart.PartNameInfo.Category));
            partNameBuilder.Append(" ");
            partNameBuilder.Append(StaticPart.GetModelNameInfo(partName, StaticPart.PartNameInfo.Version));
            return partNameBuilder.ToString();
        }

        /// <summary>
        /// Sets up button events
        /// </summary>
        void SetupEvents()
        {
            partButton.onClick.AddListener(delegate { PartSelected(this); });
        }

        /// <summary>
        /// Disposes button events
        /// </summary>
        void DisposeEvents()
        {
            partButton.onClick.RemoveAllListeners();
            PartSelected = null;
        }
    }
}
