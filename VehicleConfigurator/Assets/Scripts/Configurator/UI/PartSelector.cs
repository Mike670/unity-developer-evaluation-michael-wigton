﻿using System.Collections.Generic;
using UnityEngine;
using Atlatl.Test.Part;
using Atlatl.Test.Util;

namespace Atlatl.Test.UI
{
    /// <summary>
    /// Manages the part selection part of the vehicle configurator
    /// </summary>
    public class PartSelector : MonoBehaviour
    {
        public CanvasGroup partSelector;
        public GameObject partItemTemplate;
        public Transform partItemRoot;
        public ButtonStateColors buttonStateColors;

        /// <summary>
        /// Button state colors for partUiItems
        /// </summary>
        [System.Serializable]
        public class ButtonStateColors
        {
            public Color normalState = Color.grey;
            public Color selectedState = Color.yellow;
            public Color incompatibleState = Color.red;
        }

        PartUiItem selectedPartButton;
        List<PartUiItem> currentPartUiItems = new List<PartUiItem>();
        const float selectorFadeTime = 0.3f;

        void Start()
        {
            SetupEvents();
        }

        void OnDispose()
        {
            DisposeEvents();
        }

        /// <summary>
        /// Setup incoming events from the configurator
        /// </summary>
        void SetupEvents()
        {
            VehicleConfigurator.instance.partSelected += OnPartSelected;
            VehicleConfigurator.instance.partDeselected += PartDeselected;
        }

        /// <summary>
        /// Disposes of events linked in SetupEvents()
        /// </summary>
        void DisposeEvents()
        {
            VehicleConfigurator.instance.partSelected -= OnPartSelected;
            VehicleConfigurator.instance.partDeselected -= PartDeselected;
        }

        /// <summary>
        /// Called when a part is selected, 
        /// fades in part selector ui and populates it
        /// </summary>
        /// <param name="part">Part that has been selected</param>
        void OnPartSelected(PartBase part)
        {
            if (partSelector.alpha != 1)
            {
                partSelector.FadeCanvasGroup(this, 0, 1, selectorFadeTime);
                PopulateSelectionGrid(part);
            }
        }

        /// <summary>
        /// Called when a part is deselected,
        /// fades out part selector ui and cleans up the partUiItems
        /// </summary>
        /// <param name="part"></param>
        void PartDeselected(PartBase part)
        {
            partSelector.FadeCanvasGroup(this, 1, 0, selectorFadeTime);
            ClearParts();
        }

        /// <summary>
        /// Fills the part selector grid with the category of the selected part
        /// </summary>
        /// <param name="selectedPart">Part which contains the category to populate with</param>
        void PopulateSelectionGrid(PartBase selectedPart)
        {
            List<PartBase> activeParts = VehicleConfigurator.instance.GetActiveParts();
            
            foreach (var part in selectedPart.Category.parts)
            {
                GameObject newPart = Instantiate(partItemTemplate);
                newPart.transform.SetParent(partItemRoot, false);
                newPart.name = part.name;
                PartUiItem partUiItem = newPart.GetComponent<PartUiItem>();
                partUiItem.SetColors(buttonStateColors);

                foreach (var incompatiblePart in part.incompatibleParts)
                {
                    if (activeParts.Contains(incompatiblePart)) partUiItem.MarkAsIncompatible();
                }

                if (part == selectedPart)
                {
                    partUiItem.MarkAsCurrentPart();
                    selectedPartButton = partUiItem; 
                }

                if (partUiItem == null) Debug.LogError("Part UI Item does not have PartUiItem component", gameObject);
                partUiItem.Part = part;
                partUiItem.PartSelected += OnNewUiPartSelected;
                currentPartUiItems.Add(partUiItem);
            }
        }

        /// <summary>
        /// Removes all parts from part selector ui
        /// </summary>
        void ClearParts()
        {
            foreach (var part in currentPartUiItems)
            {
                part.PartSelected -= OnNewUiPartSelected;
                Destroy(part.gameObject);
            }

            currentPartUiItems.Clear();
        }

        /// <summary>
        /// Called when the user selects a new part,
        /// Deselects the current part and changes the part to the new one
        /// </summary>
        /// <param name="partButton">Part to change to</param>
        void OnNewUiPartSelected(PartUiItem partButton)
        {
            selectedPartButton.MarkAsNormal();
            if (VehicleConfigurator.instance.selectedPart != null) VehicleConfigurator.instance.selectedPart.Deselect();
            partButton.Part.Category.SetPart(partButton.Part);
            partButton.MarkAsCurrentPart();
            selectedPartButton = partButton;
        }
    }
}
