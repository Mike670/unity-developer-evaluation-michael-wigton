﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Atlatl.Test.Part;
using UnityEngine.EventSystems;

namespace Atlatl.Test.UI 
{
    /// <summary>
    /// General UI manager for configurator
    /// </summary>
    public class ConfiguratorUI : MonoBehaviour
    {
        /// <summary>
        /// Checks if user input is over the UI
        /// </summary>
        /// <returns>true if user input is over the UI</returns>
        public bool IsUiBlockingInput()
        {
            return EventSystem.current.IsPointerOverGameObject(); 
        }

        /// <summary>
        /// Event handler for save button press, opens the save dialog
        /// </summary>
        public void OnSaveConfigurationButtonPressed()
        {
            VehicleConfigurator.instance.DeselectSelectedPart(); 
            var window = ModalWindow.instance.ShowStringInput("Save Configuration", "Configuration Name..."); 
            window.Finished += OnSaveConfigurationWindowFinished; 
        }

        /// <summary>
        /// Event handler for save dialog return, saves the config
        /// </summary>
        /// <param name="result"></param>
        void OnSaveConfigurationWindowFinished(ModalWindow.ModalResult<string> result)
        {
            if (result.result != ModalWindow.ModalResult<string>.Result.Ok) return;
            VehicleConfigurator.instance.SaveConfiguration(result.data);
        }

        /// <summary>
        /// Event handler for load config button pressed, opens the load dialog
        /// </summary>
        public void OnLoadConfigurationButtonPressed()
        {
            VehicleConfigurator.instance.DeselectSelectedPart(); 
            VehicleConfiguratorSqliteData sql = new VehicleConfiguratorSqliteData();
            sql.OpenConnection();
            List<string> configs = sql.GetConfigurations();

            var window = ModalWindow.instance.ShowListSelectorDialog("Load Configuration", configs);
            window.Finished += OnLoadConfigurationFinished;
        }

        /// <summary>
        /// Event Handler for load dialog return, loads the config
        /// </summary>
        /// <param name="results"></param>
        void OnLoadConfigurationFinished(ModalWindow.ModalResult<string> results)
        {
            if (results.result != ModalWindow.ModalResult<string>.Result.Ok) return;
            
            string config = results.data;
            if(String.IsNullOrEmpty(config))
            {
                Debug.LogError("Config name is empty", gameObject);
                return;
            }

            VehicleConfigurator.instance.LoadConfiguration(config);
        }

        /// <summary>
        /// Event handler for random config button pressed 
        /// </summary>
        public void OnRandomConfigurationPressed()
        {
            VehicleConfigurator.instance.RandomConfiguration();
        } 
    }
}
