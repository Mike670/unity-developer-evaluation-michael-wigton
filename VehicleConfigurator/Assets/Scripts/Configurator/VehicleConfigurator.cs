﻿using UnityEngine;
using System;   
using System.Collections.Generic;
using Atlatl.Test.Part;
using System.Linq;
using Atlatl.Test.Util;
using System.Data;
using Mono.Data.SqliteClient;

namespace Atlatl.Test
{
    /// <summary>
    /// Manages the creation/setup and customization of the vehicle
    /// </summary>
    public class VehicleConfigurator : MonoBehaviour
    {        
        public static VehicleConfigurator instance;
        public UI.ConfiguratorUI configuratorUI;

        public Action<PartBase> partSelected;
        public Action<PartBase> partDeselected;
 
        public List<GameObject> models;
        public List<Material> milMaterials;
        public Material glassMaterial;
        public GameObject vehicleRoot;
        public UserInput.UserInput userInput;
        public Material selectedMaterial;
        public List<IncompatibleParts> incompatibleParts;
        [HideInInspector] public PartBase selectedPart;

        List<PartCategory> categories;
        List<PartBase> allParts;

        VehicleConfiguratorSqliteData sqlData;

        void Awake()
        {
            if (instance == null) instance = this;
            categories = SetupParts(models, vehicleRoot, out allParts);
            incompatibleParts.ForEach(x => x.SetupIncompatibleParts(allParts));
            SetupEvents();
            sqlData = new VehicleConfiguratorSqliteData();
        }

        void OnDestroy()
        {
            DisposeEvents();
        }
        
        /// <summary>
        /// Gets all active parts in all categories in the configuration
        /// </summary>
        public List<PartBase> GetActiveParts()
        {
            List<PartBase> activeParts = new List<PartBase>();
            categories.ForEach(x => activeParts.Add(x.activePart));
            return activeParts;
        }

        /// <summary>
        /// Deeselects the active part if there is one
        /// </summary>
        public void DeselectSelectedPart()
        {
            if (selectedPart != null)
            {
                selectedPart.Deselect();
                partDeselected(selectedPart);
                selectedPart = null;
            }

        }

        /// <summary>
        /// Randomizes the parts configuration
        /// </summary>
        public void RandomConfiguration()
        {
            DeselectSelectedPart();
            categories.ForEach(x => x.SetRandomPart());
        }
        
        /// <summary>
        /// Sets up incoming events
        /// </summary>
        void SetupEvents()
        {
            userInput.SelectInputPressed += OnSelectInput;
        }

        /// <summary>
        /// Disposes of inputs created on setup
        /// </summary>
        void DisposeEvents()
        {
            userInput.SelectInputPressed -= OnSelectInput;
        }

        /// <summary>
        /// Saves the current configuration to disk
        /// </summary>
        /// <param name="configName">Name of the config to save</param>
        public void SaveConfiguration(string configName)
        {
            sqlData.OpenConnection();
            sqlData.SaveConfiguration(categories, configName);
            sqlData.CloseConnection();
        }

        /// <summary>
        /// Loads the config specified into the configurator
        /// </summary>
        /// <param name="configName">Name of the config to load</param>
        public void LoadConfiguration(string configName)
        {
            DeselectSelectedPart();
            sqlData.OpenConnection();
            
            Dictionary<string, string> configuration = sqlData.GetConfiguration(configName);
            var configEnum = configuration.Keys.GetEnumerator();
            while (configEnum.MoveNext())
            {
                PartCategory category = GetCategory(categories, configEnum.Current);
                category.SetPart(configuration[configEnum.Current]);
            }

            sqlData.CloseConnection();
        }

        /// <summary>
        /// Gets a category from the supplied list using a string name
        /// </summary>
        /// <param name="categories">Categories to search</param>
        /// <param name="name">Name of category to search for</param>
        /// <returns>Category found matching name, null if not found</returns>
        PartCategory GetCategory(List<PartCategory> categories, string name)
        {
            return categories.Single(x => x.Name == name);
        }

        /// <summary>
        /// Event handler when the user does an input action
        /// </summary>
        /// <param name="screenCoords">Screen coordinates vector, vector3 for convenience, z is 0</param>
        void OnSelectInput(Vector3 screenCoords)
        {
            if (configuratorUI.IsUiBlockingInput()) return;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(screenCoords);
            if (Physics.Raycast(ray, out hit))
            {
                ISelectable obj = hit.collider.gameObject.GetComponentTransverseUp<ISelectable>();
                if (obj == null) return;

                PartBase partBase = (PartBase)obj;
                if (partBase != null)
                {
                    if (partBase == selectedPart) return;
                    if (selectedPart != null)
                    {
                        selectedPart.Deselect();
                        partDeselected(selectedPart);
                    }

                    partBase.Select();
                    partSelected(partBase);
                    selectedPart = partBase;
                }
                else
                {
                    obj.Select();
                }
            }
            else
            {
                if (selectedPart != null)
                {
                    selectedPart.Deselect();
                    partDeselected(selectedPart);
                    selectedPart = null;
                }


            }
        }

        /// <summary>
        /// Using the objects from the param parts, sets up the parts in the scene for user customization
        /// by parsing the names of the parts for categories, creating category objects in the scene under
        /// the partsRoot param, creating the part object in the scene, adding the part to the category,
        /// and show only the first configuration of parts
        /// </summary>
        /// <param name="parts">Parts in project view to setup</param>
        /// <param name="partsRoot">The parent object of the new categories and parts</param>
        /// <returns>The newly created categories</returns>
        List<PartCategory> SetupParts(List<GameObject> parts, GameObject partsRoot, out List<PartBase> allParts)
        {
            parts.Sort((a, b) => a.name.CompareTo(b.name));

            allParts = new List<PartBase>();
            List<PartCategory> categories = new List<PartCategory>();
            foreach (var part in parts)
            {
                string partCategory = StaticPart.GetModelNameInfo(part.name, StaticPart.PartNameInfo.Category);
                string milVersionStr = StaticPart.GetModelNameInfo(part.name, StaticPart.PartNameInfo.MilVersion);
                int milVersion = 1;
                if(!int.TryParse(milVersionStr, out milVersion))
                {
                    Debug.LogWarning("Part: " + part.name + " has invalid mil version, defaulting to 1");
                }

                if(!CategoryExists(categories, partCategory))
                {
                    categories.Add(CreateCategory(partCategory, partsRoot));
                }

                PartBase partObj = CreatePart(part, milVersion);
                allParts.Add(partObj);
                SetPartCategory(partObj, partCategory, categories);
            }

            foreach (var categoryItem in categories)
            {
                categoryItem.SetPart(0);
            }

            return categories;
        }

        /// <summary>
        /// Creates a part in the scene from a model in the project. Applies the
        /// appropriate material to objects according to the milVersion
        /// </summary>
        /// <param name="part">Model from the project</param>
        /// <param name="milVersion"></param>
        /// <returns>Newly created part</returns>
        PartBase CreatePart(GameObject part, int milVersion)
        {
            GameObject partObj = Instantiate(part);
            partObj.name = part.name;
            PartBase partBase = partObj.AddComponent<StaticPart>();
            partBase.MilVersion = milVersion;
            Material material = GetMilMaterial(milVersion);

            var list = partObj.GetComponentsInChildren<Renderer>().ToList();
            foreach (var item in list)
            {
                if (item.name.ToLower().Contains("glass")) item.material = glassMaterial;
                else item.material = material;
            }
            return partBase;
        }

        /// <summary>
        /// Gets the correct mil material from the mil version
        /// </summary>
        /// <param name="milVersion"></param>
        /// <returns></returns>
        Material GetMilMaterial(int milVersion)
        {
            return milMaterials[milVersion - 1];
        }

        /// <summary>
        /// Takes a part in the scene and moves it into the appropriate category
        /// </summary>
        /// <param name="part">Part from the scene</param>
        /// <param name="category">Name of the part category</param>
        /// <param name="categories">Categories in the scene</param>
        void SetPartCategory(PartBase part, string category, List<PartCategory> categories)
        {
            try
            {
                PartCategory partCategory = categories.Single(x => x.Name == category);
                part.Category = partCategory;
                part.gameObject.transform.SetParent(partCategory.gameObject.transform);
                partCategory.parts.Add(part);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Could not find matching category for part: " + part.name + "\nDetails: " + e.Message);
                return;
            }
        }

        /// <summary>
        /// Creates a new category in the scene under root
        /// </summary>
        /// <param name="name">Name for the new category</param>
        /// <param name="root">Object to use as the parent for the catagory</param>
        /// <returns></returns>
        PartCategory CreateCategory(string name, GameObject root)
        {
            GameObject categoryObj = new GameObject(name);
            categoryObj.transform.SetParent(root.transform, true);
            PartCategory category = categoryObj.AddComponent<PartCategory>();
            category.Name = name;
            return category;
        }

        /// <summary>
        /// Checks if a category already exists in the scene
        /// </summary>
        /// <param name="categories">Categories in the scene to check against</param>
        /// <param name="category">Category to check</param>
        /// <returns></returns>
        bool CategoryExists(List<PartCategory> categories, string category)
        {
            foreach (var categoryItem in categories)
            {
                if (categoryItem.Name == category) return true;
            }

            return false;
        }
    }
}
