﻿using UnityEngine;
using System.Collections;

namespace Atlatl.Test.Part
{
    /// <summary>
    /// A part which has no animation or interaction
    /// </summary>
    public class StaticPart : PartBase
    {
        /// <summary>
        /// Index of info in part name to access
        /// </summary>
        public enum PartNameInfo
        {
            MilVersion = 1,
            Category = 2,
            Version = 3
        }

        /// <summary>
        /// Gets info from a partname which is formatted: mil_milVersion_category_x_x
        /// </summary>
        /// <param name="partName">string to parse</param>
        /// <param name="infoIndex">index of info, use nameIndex consts</param>
        /// <returns></returns>
        public static string GetModelNameInfo(string partName, PartNameInfo info)
        {
            return partName.Split('_')[(int)info];
        }
    }
}