﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Atlatl.Test.Part
{
    /// <summary>
    /// Manages the incompatable parts per part
    /// </summary>
    [Serializable]
    public class IncompatibleParts
    {
        public string Name = "Part";
        public GameObject partModel;
        public List<GameObject> incompatiblePartModels;

        /// <summary>
        /// Gets and sets all incompatable parts related to the model assigned to class
        /// </summary>
        /// <param name="allParts">List of all parts in the configurator</param>
        public void SetupIncompatibleParts(List<PartBase> allParts)
        {
            PartBase part = GetPartFromModel(allParts, partModel.name);
            List<PartBase> incompatibleParts = GetIncompatiblePartsFromModels(allParts, incompatiblePartModels);
            AddIncompatablePartsToParts(incompatibleParts, part);
        }

        /// <summary>
        /// Gets List of incompatible parts from the incompatiblePartModels list by comparing the names 
        /// </summary>
        /// <param name="allParts"></param>
        List<PartBase> GetIncompatiblePartsFromModels(List<PartBase> allParts, List<GameObject> incompatiblePartModels)
        {
            List<PartBase> incompatableParts = new List<PartBase>();

            foreach (var partObj in incompatiblePartModels)
            {
                PartBase newPart = allParts.Single(x => partObj.name == x.name);
                incompatableParts.Add(newPart);
            }

            return incompatableParts;
        }

        /// <summary>
        /// Gets PartBase by comparing model name
        /// </summary>
        /// <param name="allParts">all parts in the configurator</param>
        /// <param name="modelName">name of the model to look for</param>
        PartBase GetPartFromModel(List<PartBase> allParts, string modelName)
        {
            return allParts.Single(x => x.name == modelName);
        }

        /// <summary>
        /// Adds incompatible parts to all parts affected
        /// </summary>
        /// <param name="incompatibleParts"></param>
        /// <param name="part"></param>
        void AddIncompatablePartsToParts(List<PartBase> incompatibleParts, PartBase part)
        {
            part.incompatibleParts.AddRange(incompatibleParts);
            incompatibleParts.ForEach(x => x.incompatibleParts.Add(part));
        }
    } 
}
