﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Atlatl.Test.Util;

namespace Atlatl.Test.Part
{
    /// <summary>
    /// Base part for configurator, allows unique features from derived parts
    /// </summary>
    public abstract class PartBase : MonoBehaviour, ISelectable, IComparable<PartBase> 
    {
        public List<PartBase> incompatibleParts = new List<PartBase>();

        public bool IsSelected { private set; get; }
        public int MilVersion { get; set; }
        public PartCategory Category { get; set; }

        List<Material> originalMaterials;

        void Start()
        {
            Init();
        }

        /// <summary>
        /// Inital setup for part
        /// </summary>
        public virtual void Init()
        {
            originalMaterials = gameObject.GetChildMaterials();
            SetupColliders();
        }

        /// <summary>
        /// Adds colliders to all meshes in children
        /// </summary>
        public virtual void SetupColliders()
        {
            MeshFilter[] meshes = GetComponentsInChildren<MeshFilter>();
            foreach (var mesh in meshes)
            {
                MeshCollider collider = mesh.gameObject.AddComponent<MeshCollider>();
                collider.sharedMesh = mesh.mesh;
            }
        }

        /// <summary>
        /// Visual feedback for part selection
        /// </summary>
        public virtual void Select()
        {
            //TODO make multiple select materials depending on all materials
            VehicleConfigurator.instance.selectedMaterial.mainTexture = originalMaterials[0].mainTexture;
            VehicleConfigurator.instance.selectedMaterial.SetTexture("_BumpMap", originalMaterials[0].GetTexture("_BumpMap"));
            gameObject.AssignChildMaterials(VehicleConfigurator.instance.selectedMaterial);
        }

        /// <summary>
        /// Visual feedback for part deselection
        /// </summary>
        public virtual void Deselect()
        {
            gameObject.AssignChildMaterials(originalMaterials);
        }

        /// <summary>
        /// Sort implementation using the gameobject name
        /// </summary>
        public int CompareTo(PartBase other)
        {
            return name.CompareTo(other.name);
        }
    }
}