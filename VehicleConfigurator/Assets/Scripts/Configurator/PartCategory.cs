﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Atlatl.Test.Part;
using System.Linq;

namespace Atlatl.Test
{
    /// <summary>
    /// A collection of parts grouped by name. Example: Wheels
    /// </summary>
    public class PartCategory : MonoBehaviour, IComparable<PartCategory> 
    {
        public string Name;
        public List<PartBase> parts = new List<PartBase>();
        public PartBase activePart;

        /// <summary>
        /// Sets the current active part by the component
        /// </summary>
        /// <param name="part"></param>
        public void SetPart(PartBase part)
        {
            HideAllParts();
            part.gameObject.SetActive(true);
            activePart = part;
        }

        /// <summary>
        /// Sets the current active part by the index
        /// </summary>
        /// <param name="index"></param>
        public void SetPart(int index)
        {
            HideAllParts();
            parts[index].gameObject.SetActive(true);
            activePart = parts[index];
        }

        public void SetPart(string partName)
        {
            HideAllParts();
            PartBase part = parts.Single(x => x.name == partName);

            if(part == null)
            {
                Debug.LogWarning(String.Format("Part {0} not found in category {1}, setting to first part", partName, Name));
                SetPart(0);
                return;
            }

            SetPart(part);
        }

        /// <summary>
        /// Hide all parts in the category
        /// </summary>
        public void HideAllParts()
        {
            parts.ForEach(x => x.gameObject.SetActive(false));
        }

        /// <summary>
        /// Sets a random part in this category as the active part
        /// </summary>
        public void SetRandomPart()
        {
            SetPart(UnityEngine.Random.Range(0, parts.Count - 1));
            var allParts = VehicleConfigurator.instance.GetActiveParts();
            List<PartBase> compatibleParts = new List<PartBase>();
            bool incompatible = false;

            foreach (var part in parts)
            {
                foreach (var incomptablePart in part.incompatibleParts)
                {
                    if (allParts.Contains(incomptablePart))
                    {
                        incompatible = true;
                        break;
                    }
                }

                if (!incompatible) compatibleParts.Add(part);
                incompatible = false;
            }

            SetPart(compatibleParts[UnityEngine.Random.Range(0, compatibleParts.Count - 1)]);
        } 

        /// <summary>
        /// Sort implementation using category name
        /// </summary>
        public int CompareTo(PartCategory other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}
