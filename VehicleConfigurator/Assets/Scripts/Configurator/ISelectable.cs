﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlatl.Test 
{
    /// <summary>
    /// A contract that an object can be selected in the scene
    /// </summary>
    public interface ISelectable
    {
        void Select();
        void Deselect();
    }
}
