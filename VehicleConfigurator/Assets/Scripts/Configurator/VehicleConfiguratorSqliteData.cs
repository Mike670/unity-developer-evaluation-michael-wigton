﻿using System;
using System.Collections.Generic;
using System.Data;
using Mono.Data.SqliteClient;
using Atlatl.Test.Part;
using System.Linq;

namespace Atlatl.Test 
{
    /// <summary>
    /// Manages the configuration saving and loading to sqlite databases
    /// </summary>
    public class VehicleConfiguratorSqliteData
    {
        const string databaseFileName = "URI=file:Configurations.sqlite";
        const string categoryColumn = "category";
        const string partColumn = "part";

        IDbConnection dbCon;
        IDataReader dbReader;

        /// <summary>
        /// Establishes the connection to the database, must be called first
        /// </summary>
        public void OpenConnection()
        {
            dbCon = new SqliteConnection(databaseFileName);
            dbCon.Open();
        }

        /// <summary>
        /// Closes the connection to the database, must be called last
        /// </summary>
        public void CloseConnection()
        {
            dbCon.Close();
            dbCon.Dispose();
            dbCon = null;
        }

        /// <summary>
        /// Create a new table for a configuration
        /// </summary>
        /// <param name="configName">Name of the table to create</param>
        void CreateConfigTable(string configName)
        {
            string sql = String.Format("CREATE TABLE config_{0} ({1} VARCHAR(30), {2} VARCHAR(50))", 
                                        configName, categoryColumn, partColumn);

            IDbCommand dbCmd = dbCon.CreateCommand();
            dbCmd.CommandText = sql;
            dbCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Checks if a config table already exists in the database
        /// </summary>
        /// <param name="configName">name of the config table to check</param>
        /// <returns>True if config table exists</returns>
        public bool ConfigExists(string configName)
        {
            string sql = String.Format("SELECT name FROM sqlite_master WHERE  type='table' AND name='config_{0}'", configName);
            IDbCommand dbCmd = dbCon.CreateCommand();
            dbCmd.CommandText = sql;
            var reader = dbCmd.ExecuteReader();
            return reader.Read();
        }

        /// <summary>
        /// Writes the config to that database
        /// </summary>
        /// <param name="categories">Part categories from the config to write</param>
        /// <param name="configName">Name of the config table to write to</param>
        public void SaveConfiguration(List<PartCategory> categories, string configName)
        {
            configName = EscapeConfigName(configName);

            if (ConfigExists(configName))
            {
                UpdateConfiguration(categories, configName);
            }
            else
            {
                NewConfiguration(categories, configName);
            }
        }

        /// <summary>
        /// Escapes the config name into a format which strips out anything
        /// that is not a number, letter, or underscore
        /// </summary>
        string EscapeConfigName(string configName)
        {
            configName = configName.Replace(' ', '_');
            configName = new String(configName.Where(x => x == '_' || Char.IsLetter(x) || Char.IsNumber(x)).ToArray());
            return configName;
        }
        

        /// <summary>
        /// Writes a new configuration table to the database
        /// </summary>
        /// <param name="categories">Part categories from the config to write</param>
        /// <param name="configName">Name of the config table to write to</param>
        void NewConfiguration(List<PartCategory> categories, string configName)
        {
            CreateConfigTable(configName);
            
            foreach (var category in categories)
            {
                string sql = String.Format("INSERT INTO config_{0} ({1}, {2}) values ('{3}', '{4}')",
                                            configName, categoryColumn, partColumn, category.Name, category.activePart.name);

                IDbCommand command = dbCon.CreateCommand();
                command.CommandText = sql;
                command.ExecuteNonQuery();

            }
        }


        /// <summary>
        /// Updates and existing configuration table, if a category exists it is updated, 
        /// if not, a new one is created
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="configName"></param>
        void UpdateConfiguration(List<PartCategory> categories, string configName)
        {
            foreach (var category in categories)
            {
                string sql = String.Format("select 1 from config_{0} where {1} = '{2}'",
                                            configName, categoryColumn, category.Name);

                IDbCommand command = dbCon.CreateCommand();
                command.CommandText = sql;
                var reader = command.ExecuteReader();

                //If category exists in table, update it, create new row if now
                if (reader.Read())
                {
                    string sqlCmd = String.Format("update config_{0} set {1}='{2}' where {3}='{4}'",
                                                    configName, partColumn, category.activePart.name, categoryColumn, category.Name);
                    IDbCommand cmd = dbCon.CreateCommand();
                    cmd.CommandText = sqlCmd;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    string sqlCmd = String.Format("INSERT INTO config_{0} ({1}, {2}) values ('{3}', '{4}')",
                                                configName, categoryColumn, partColumn, category.Name, category.activePart.name);

                    IDbCommand cmd = dbCon.CreateCommand();
                    cmd.CommandText = sqlCmd;
                    cmd.ExecuteNonQuery();
                } 
            }
        }

        /// <summary>
        /// Gets a configuration from the database and puts it into a dict,
        /// the key is the category and the value is the part
        /// </summary>
        public Dictionary<string, string> GetConfiguration(string configName)
        {
            Dictionary<string, string> configs = new Dictionary<string, string>();

            string sql = String.Format("select {0}, {1} from config_{2}",
                                        categoryColumn, partColumn, configName);
            
            IDbCommand command = dbCon.CreateCommand();
            command.CommandText = sql;
            var reader = command.ExecuteReader();
            while(reader.Read())
            {
                string category = reader.GetString(0);
                if (String.IsNullOrEmpty(category)) continue;

                string part = reader.GetString(1);
                if (String.IsNullOrEmpty(part)) continue;

                configs.Add(category, part);
            }

            return configs;
        }

        /// <summary>
        /// Gets a list of configurations in the database
        /// </summary>
        public List<string> GetConfigurations()
        {
            List<string> configs = new List<string>();

            string sql = "select name from sqlite_master where type = \"table\"";
            IDbCommand command = dbCon.CreateCommand();
            command.CommandText = sql;
            var reader = command.ExecuteReader();

            while(reader.Read())
            {
                string config = reader.GetString(0);
                if(config.Contains("config_"))
                {
                    configs.Add(config.Substring(config.IndexOf('_') + 1));
                }
            }

            return configs;
        }
    }
}
