﻿using UnityEngine;
using UnityEngine.UI;

namespace Atlatl.Test.UI 
{
    public class ListItem : MonoBehaviour
    {
        [SerializeField]
        Text itemText;

        [SerializeField]
        Toggle toggle;

        ListSelectorWindow parent;
        string data;

        public void SetText(string text)
        {
            if(itemText == null)
            {
                Debug.LogError("Text is not assigned", gameObject);
                return;
            }

            itemText.text = text;
        }

        public void SetData(string data)
        {
            this.data = data;
        }

        public string GetData()
        {
            return data;
        }

        public string GetText()
        {
            if(itemText == null)
            {
                Debug.LogError("Text is not assigned", gameObject);
                return System.String.Empty;
            }

            return itemText.text;
        }

        public void SetParent(ListSelectorWindow parent)
        {
            this.parent = parent;
            this.toggle.group = parent.group;
        }
    }
}
