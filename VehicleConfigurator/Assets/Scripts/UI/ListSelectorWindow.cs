﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Atlatl.Test.UI 
{
    public class ListSelectorWindow : MonoBehaviour
    {
        public Action<ModalWindow.ModalResult<string>> Finished;
        public ListItem listItemTemplate;
        public Transform contentRoot;
        public Button okButton;
        public Button cancelButton;
        public Text title;
        public ToggleGroup group;

        void Start()
        {
            SetupEvents();
        }

        void OnDestroy()
        {
            DisposeEvents();
        }

        /// <summary>
        /// Setup incoming events
        /// </summary>
        void SetupEvents()
        {
            okButton.onClick.AddListener(OnOkButtonPressed);
            cancelButton.onClick.AddListener(OnCancelButtonPressed);
        }

        /// <summary>
        /// Dispose events created in setupEvents
        /// </summary>
        void DisposeEvents()
        {
            okButton.onClick.RemoveListener(OnOkButtonPressed);
            cancelButton.onClick.RemoveListener(OnCancelButtonPressed);
        }

        /// <summary>
        /// Sets title of the dialog
        /// </summary>
        public void SetTitle(string title)
        {
            this.title.text = title;
        }

        public void AddItems(List<string> items, bool unescape)
        {
            foreach (var item in items)
            {
                string itemName = item;
                
                if(unescape)
                {
                    itemName = itemName.Replace('_', ' ');
                }

                GameObject newItem = Instantiate(listItemTemplate.gameObject);
                newItem.transform.SetParent(contentRoot);
                newItem.name = itemName;
                ListItem listItem = newItem.GetComponent<ListItem>();

                if(listItem == null)
                {
                    Debug.LogError("No list item attached to template", gameObject);
                    Destroy(newItem);
                    return;
                }

                listItem.SetParent(this);
                listItem.SetData(item);
                listItem.SetText(itemName);

            }
        }
        

        void OnOkButtonPressed()
        {
            ModalWindow.ModalResult<string> result = new ModalWindow.ModalResult<string>();
            result.result = ModalWindow.ModalResult<string>.Result.Ok;
            ListItem item = GetSelectedItem();
            if (item == null) return;

            result.data = item.GetData();
            if(result.data == null)
            {
                Debug.LogWarning("Could not get selected item from list", gameObject);
            }

            Finished(result);
            Destroy(gameObject);
        }

        ListItem GetSelectedItem()
        {
            IEnumerable<Toggle> toggles = group.ActiveToggles();
            var toggleEnum = toggles.GetEnumerator(); 
            while(toggleEnum.MoveNext())
            {
                if (toggleEnum.Current != null)
                {
                    ListItem item = toggleEnum.Current.gameObject.GetComponent<ListItem>();
                    if (item != null) return item;
                }
            }

            return null;
        }

        void OnCancelButtonPressed()
        {
            ModalWindow.ModalResult<string> result = new ModalWindow.ModalResult<string>();
            result.result = ModalWindow.ModalResult<string>.Result.Cancel;
            result.data = null;
            Finished(result);
            Destroy(gameObject);
        }
    }
}
