﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Atlatl.Test.UI
{ 
/// <summary>
/// Manager class for dialog/modal windows
/// </summary>
    public class ModalWindow : MonoBehaviour
    {
        /// <summary>
        /// Result to return from a dialog/modal window,
        /// Generic data can be returned from the dialog/modal
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class ModalResult<T>
        {
            public enum Result
            {
                Ok,
                Cancel
            }

            public Result result;
            public T data;
        }

        public Transform windowRoot;
        public GameObject inputBlocker;
        public StringInputWindow stringInputPrefab;
        public ListSelectorWindow listSelectorPrefab;

        public static ModalWindow instance;

        public bool InputBlocked
        {
            get
            {
                return inputBlocker.activeSelf;
            }
        }

        void Awake()
        {
            if (instance == null) instance = this;
        }

        /// <summary>
        /// Shows a dialog for getting a string from the user
        /// </summary>
        /// <param name="title">Title of the dialog</param>
        /// <param name="hintText">Hint text on the input box</param>
        /// <returns></returns>
        public StringInputWindow ShowStringInput(string title, string hintText)
        {
            ActivateInputBlocker();

            GameObject newWindowObj = Instantiate(stringInputPrefab.gameObject);
            newWindowObj.transform.SetParent(windowRoot, false);
            StringInputWindow inputWindow = newWindowObj.GetComponent<StringInputWindow>();
            inputWindow.SetHelpText(hintText);
            inputWindow.SetTitle(title);

            if (inputWindow == null)
            {
                Debug.LogError("Could not get StringInputWindow component", gameObject);
                return null;
            }

            inputWindow.Finished += OnClose;
            return inputWindow;

        }

        /// <summary>
        /// Shows a dialog for selecting from a list of strings
        /// </summary>
        /// <param name="title">Title of the dialog</param>
        /// <param name="items">list items</param>
        /// <returns></returns>
        public ListSelectorWindow ShowListSelectorDialog(string title, List<string> items)
        {
            ActivateInputBlocker();

            GameObject newWindowObj = Instantiate(listSelectorPrefab.gameObject);
            newWindowObj.transform.SetParent(windowRoot, false);
            ListSelectorWindow listSelector = newWindowObj.GetComponent<ListSelectorWindow>();
            if(listSelector == null)
            {
                Debug.LogError("Could not get list selector from template", gameObject);
                Destroy(newWindowObj);
                return null;
            }

            listSelector.SetTitle(title);
            listSelector.AddItems(items, true);
            listSelector.Finished += OnClose;
            return listSelector;
        }

        /// <summary>
        /// Blocks user input behind dialogs
        /// </summary>
        void ActivateInputBlocker()
        {
            inputBlocker.SetActive(true);
        }

        /// <summary>
        /// Generic close handler for dialogs
        /// </summary>
        /// <param name="data"></param>
        void OnClose(object data)
        {
            inputBlocker.SetActive(false);
        }

    }
}
