﻿using System;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

namespace Atlatl.Test.UI
{ 
/// <summary>
/// Dialog window for getting a string from the user
/// </summary>
    public class StringInputWindow : MonoBehaviour
    {
        public Action<ModalWindow.ModalResult<string>> Finished;

        public Button okButton;
        public Button cancelButton;
        public InputField input;
        public Text title;

        void Start()
        {
            SetupEvents();
        }

        void OnDestroy()
        {
            DisposeEvents();
        }

        /// <summary>
        /// Setup incoming events
        /// </summary>
        void SetupEvents()
        {
            okButton.onClick.AddListener(OnOkButtonPressed);
            cancelButton.onClick.AddListener(OnCancelButtonPressed);
        }

        /// <summary>
        /// Dispose events created in setupEvents
        /// </summary>
        void DisposeEvents()
        {
            okButton.onClick.RemoveListener(OnOkButtonPressed);
            cancelButton.onClick.RemoveListener(OnCancelButtonPressed);
        }

        /// <summary>
        /// Sets title of the dialog
        /// </summary>
        public void SetTitle(string title)
        {
            this.title.text = title;
        }

        /// <summary>
        /// Sets input field help text
        /// </summary>
        public void SetHelpText(string text)
        {
            ((Text)input.placeholder).text = text;
        }

        /// <summary>
        /// Event that handles when the ok button is pressed,
        /// sends out the text data then closes the dialog
        /// </summary>
        void OnOkButtonPressed()
        {
            if (string.IsNullOrEmpty(input.text)) return;

            ModalWindow.ModalResult<string> result = new ModalWindow.ModalResult<string>();
            result.result = ModalWindow.ModalResult<string>.Result.Ok;
            result.data = input.text;
            Finished(result);
            Destroy(gameObject);
        }

        /// <summary>
        /// Event that handles when the cancel button is pressed,
        /// sends out the cancel signal then closes the dialog
        /// </summary>
        void OnCancelButtonPressed()
        {
            ModalWindow.ModalResult<string> result = new ModalWindow.ModalResult<string>();
            result.result = ModalWindow.ModalResult<string>.Result.Cancel;
            result.data = null;
            Finished(result);
            Destroy(gameObject);
        }
    }
}
