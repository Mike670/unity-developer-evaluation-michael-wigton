﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.EventSystems;
using Atlatl.Test.UI;

namespace Atlatl.Test.UserInput
{
    /// <summary>
    /// Handles all input from the user and fire events for actions
    /// Setup for PC mouse input
    /// </summary>
    public class UserInput : MonoBehaviour
    {
        [SerializeField]
        OrbitCamera mouseOrbit;

        /// <summary>
        /// Triggered when the mouse has moved with the right button pressed, sends mouse delta
        /// </summary>
        public Action<Vector2> OrbitAction;

        /// <summary>
        /// Triggered when mouse is scrolled, sends scroll delta
        /// </summary>
        public Action<float> ZoomAction;

        /// <summary>
        /// Triggered when the input for selecting parts in the scene is used, sends screen cords
        /// </summary>
        public Action<Vector3> SelectInputPressed;

        Vector3 lastMousePos;
        Vector2 lastMouseDelta;
        float lastScrollDelta;

        void Update()
        {
            if (ModalWindow.instance.InputBlocked) return;
            if (Input.GetMouseButton(1) && MouseMoved()) OrbitAction(lastMouseDelta);
            if (MouseScrolled()) ZoomAction(lastScrollDelta);
            if (Input.GetMouseButtonDown(0)) SelectInputPressed(Input.mousePosition);
        }

        /// <summary>
        /// Check if the mouse has moved since the last frame
        /// </summary>
        /// <returns></returns>
        bool MouseMoved()
        {
            if (Input.mousePosition == lastMousePos) return false;
            lastMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            lastMousePos = Input.mousePosition;
            return true;
        }

        /// <summary>
        /// Checks if the mouse has scrolled since the last frame
        /// </summary>
        /// <returns></returns>
        bool MouseScrolled()
        {
            lastScrollDelta = Input.GetAxis("Mouse ScrollWheel");
            if (lastScrollDelta == 0) return false;
            return true;
        }
    }
}
