﻿using System;
using UnityEngine;
using System.Collections;

namespace Atlatl.Test.UserInput
{
    /// <summary>
    /// Orbit Camera that is not tied to a control scheme, uses delta inputs from 
    /// the UserInput class. Has settings for speed and zoom clamps
    /// </summary>
    public class OrbitCamera : MonoBehaviour
    {
        [SerializeField]
        UserInput userInput;

        /// <summary>
        /// Settings for the orbit grouped for inspector and saving/loading
        /// </summary>
        [Serializable]            
        public class OrbitSettings
        {
            /// <summary>
            /// Speed multipliers for input
            /// </summary>
            [Serializable]            
            public class Speed
            {
                public float xSpeed = 50;
                public float ySpeed = 170;
                public float zoom = 5;
            }

            /// <summary>
            /// Limits for clamping input to ranges
            /// </summary>
            [Serializable]            
            public class Limits
            {
                public float minDistance = 2;
                public float maxDistance = 10;
            }

            public Transform target;
            public Speed speed;
            public Limits limits;
        }

        public OrbitSettings settings;
        float xRotation;
        float yRotation;
        float targetDistance = 5;

        //Magic number to make the delta speed more managable
        const float orbitDeltaShrinker = 0.02f;

        void Start()
        {
            InitalizeTransform();
            SetupEvents();
        }
        
        void OnDestroy()
        {
            DisposeEvents();
        }
       
        /// <summary>
        /// Sets up incoming events from user input
        /// </summary>
        void SetupEvents()
        {
            userInput.OrbitAction += Orbit;
            userInput.ZoomAction += Zoom;
        }

        /// <summary>
        /// Disposes of user input events
        /// </summary>
        void DisposeEvents()
        {
            userInput.OrbitAction -= Orbit;
            userInput.ZoomAction -= Zoom;
        }

        /// <summary>
        /// Aligns internal variables to current camera transform so the camera does not jump
        /// when input is recived. Use after camera is manually moved.
        /// </summary>
        public void InitalizeTransform()
        {
            xRotation = transform.eulerAngles.y;
            yRotation = transform.eulerAngles.x;
            targetDistance = Vector3.Distance(transform.position, settings.target.position);
        }

        /// <summary>
        /// Updates the distance variable constrained withing the zoom limits in the delta amount
        /// </summary>
        /// <param name="delta"></param>
        void Zoom(float delta)
        {
            targetDistance = Mathf.Clamp(targetDistance - delta * settings.speed.zoom, settings.limits.minDistance, settings.limits.maxDistance);
            UpdateTransform();
        }

        /// <summary>
        /// Updates the orbit position variables 
        /// </summary>
        /// <param name="delta"></param>
        void Orbit(Vector2 delta)
        {
            if (settings.target)
            {
                xRotation += delta.x * settings.speed.xSpeed * targetDistance * orbitDeltaShrinker;
                yRotation -= delta.y * settings.speed.ySpeed * orbitDeltaShrinker;
                UpdateTransform();
            }
            else
            {
                Debug.LogWarning("No target assigned");
            }
        }

        /// <summary>
        /// Applys the zoom and rotation
        /// </summary>
        void UpdateTransform()
        {
            Quaternion rotation = Quaternion.Euler(yRotation, xRotation, 0);
            Vector3 negitiveDistance = new Vector3(0.0f, 0.0f, -targetDistance);
            Vector3 position = rotation * negitiveDistance + settings.target.position;

            transform.rotation = rotation;
            transform.position = position;
        }
    }
}
