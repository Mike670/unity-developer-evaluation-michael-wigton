﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atlatl.Test.Util 
{
    /// <summary>
    /// Collection of extension methods for gameobjects
    /// </summary>
    public static class GameobjectUtil
    {
        /// <summary>
        /// Gets the specified component of the gameobject, if not found, it will 
        /// look though the parent hierarchy stack untill it finds one or 
        /// it reaches the top and returns null
        /// </summary>
        /// <typeparam name="T">Component Type</typeparam>
        /// <param name="obj">object to start search</param>
        /// <returns>Component or null if nothing is found</returns>
        public static T GetComponentTransverseUp<T>(this GameObject obj)
        {
            T component = obj.GetComponent<T>();

            if (component == null)
            {
                GameObject parent = obj.transform.parent.gameObject;
                while (parent != null)
                {
                    component = parent.GetComponent<T>();
                    if (obj != null) break;
                    parent = parent.transform.parent.gameObject;
                }
            }

            return component;
        }

        /// <summary>
        /// Replaces the materials in all child objects which have a renderer
        /// </summary>
        /// <param name="obj">Root object</param>
        /// <param name="material">Material to replace with</param>
        public static void AssignChildMaterials(this GameObject obj, Material material)
        {
            List<Renderer> renderers = new List<Renderer>();
            Renderer singleRenderer = obj.GetComponent<Renderer>();
            if(singleRenderer != null ) renderers.Add(singleRenderer);
            renderers.AddRange(obj.GetComponentsInChildren<Renderer>());

            foreach (var renderer in renderers)
            {
                renderer.material = VehicleConfigurator.instance.selectedMaterial;
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    renderer.materials[i] = material;    
                }
            }
        }

        /// <summary>
        /// Replaces the materials in all child objects which have a renderer sequentially with the 
        /// materials list, use with the output of the GetChildMaterials extension method
        /// </summary>
        /// <param name="obj">Root object</param>
        /// <param name="materials">Material list from GetChildMaterials output</param>
        public static void AssignChildMaterials(this GameObject obj, List<Material> materials)
        {
            List<Renderer> renderers = new List<Renderer>();
            Renderer singleRenderer = obj.GetComponent<Renderer>();
            if(singleRenderer != null ) renderers.Add(singleRenderer);
            renderers.AddRange(obj.GetComponentsInChildren<Renderer>());
            var listEnum = materials.GetEnumerator();


            foreach (var renderer in renderers)
            {
                if(!listEnum.MoveNext()) return;
                renderer.material = listEnum.Current;

                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    if(!listEnum.MoveNext()) return;
                    renderer.materials[i] = listEnum.Current;    
                }
            }
        }

        /// <summary>
        /// Gets the renderer component from a gameobject
        /// </summary>
        public static Renderer GetRenderer(this GameObject obj)
        {
            Renderer renderer = obj.GetComponent<Renderer>();
            if (renderer == null)
            {
                Debug.LogError("No renderer on part", obj);
                return null;
            }
            return renderer;
        }

        /// <summary>
        /// Gets a list of all materials from obj and its children sequentially
        /// </summary>
        public static List<Material> GetChildMaterials(this GameObject obj)
        {
            List<Material> materials = new List<Material>();
            Renderer[] childRenderers = obj.GetComponentsInChildren<Renderer>();
            foreach (var renderer in childRenderers)
            {
                materials.Add(renderer.material);
                materials.AddRange(renderer.materials);
            }

            return materials;
        }
    }
}
