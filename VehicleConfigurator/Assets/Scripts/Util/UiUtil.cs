﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Atlatl.Test.Util
{
    /// <summary>
    /// Collection of extension methods for UI
    /// </summary>
    public static class UiUtil
    {
        /// <summary>
        /// Fades canvas group alpha
        /// </summary>
        /// <param name="monoObj">MonoBehaviour to call the Enumerator from</param>
        /// <param name="from">Where to start the fade</param>
        /// <param name="to">Where to end the fade</param>
        /// <param name="duration">time of fade in seconds</param>
        public static void FadeCanvasGroup(this CanvasGroup canvasGroup, MonoBehaviour monoObj, float from, float to, float duration)
        {
            monoObj.StartCoroutine(LerpCanvas(canvasGroup, from, to, duration));
        }

        /// <summary>
        /// Enumerator for FadeCanvasGroup 
        /// </summary>
        static IEnumerator LerpCanvas(CanvasGroup canvasGroup, float from, float to, float duration)
        {
            float t = 0;
            while (t <= 1)
            {
                t += Time.deltaTime / duration;
                canvasGroup.alpha = Mathf.Lerp(from, to, t);
                yield return new WaitForEndOfFrame();
            }
        }

    }
}
